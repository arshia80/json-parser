package com.awsemn.deserializer;

import com.awsemn.deserializer.models.JSONArray;
import com.awsemn.deserializer.models.JSONObject;
import com.awsemn.deserializer.parser.JSONParser;

public class Main {
    public static void main(String[] args) {
        String object_only = "{ \"key\":\"value\", \"key2\":\"value2\", \"key3\":10, \"key4\":true, \"key5\":1.56, \"key6\": { \"key\":1 } }";
        String array_only = "[1,2,3,4,5,6,true,\"salam\",1.432]";
        String array_in_object = "{\"action\":\"add\", \"items\":[1,2,3,4,5,6,true,\"salam\",1.432]}";
        String object_in_array = "[{\"key\":\"value\"},{\"content\":\"hello world\"}]";
        JSONArray array = new JSONParser().parse(object_in_array);
        System.out.println(array.getJSONObject(0).getString("key"));
        System.out.println(array.getJSONObject(1).getString("content"));
    }
}
